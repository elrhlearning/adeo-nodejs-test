/**
 * Check if a given array contain --count
 * @param {*} params array that contain all parameters
 */
function withCount(params) {
    return params.find(p => p.indexOf('--count') !== -1) != undefined;
}

/**
 * Check if a given array contain --filter=XX and return XX value 
 * @param {*} params array that contain all parameters
 */
function getFilter(params) {    
    var filter = params.find(p => p.indexOf('--filter=') !== -1);
    var result = [];    
    if(filter == undefined || (result = filter.split('='))[1].length === 0 )
        throw new Error('Error no given filter');
    return result[1]; 
}

/**
 * 
 * @param {*} data array that contain all data
 * @param {*} pattern search keyword
 * @param {*} outputWithCount boolean that indicat if script will get count
 */
function getFiltredData(data,pattern,outputWithCount = false) {
    var output = [];
    var cloneData = JSON.parse(JSON.stringify(data));
    cloneData.forEach((country)=> {
        // for each people
        country.people.forEach((people)=> {
            // filter animals
            people.animals = people.animals.filter(animal => animal.name.indexOf(pattern) !== -1);
            // if animals found and withCount change people name otherwise delete animals objetcs
            if(people.animals.length > 0){
                if(outputWithCount){
                    people.name = people.name.concat(` [${people.animals.length}]`);
                }
            }
            else
                delete people.animals;
        });
        // filter people
        country.people = country.people.filter(p => p.name.indexOf(pattern) !== -1 || (p.animals != undefined &&  p.animals.length > 0));
        if(country.people.length > 0){
            if(outputWithCount){
                country.name = country.name.concat(` [${country.people.length}]`);
            }
            output.push(country);
        }
    });
    return output;
}

module.exports = {
    getFilter, withCount, getFiltredData
}