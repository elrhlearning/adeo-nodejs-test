var data =  require('./data');
var helper = require('./helper');

// delete first 2 params
var params = process.argv.splice(2,2);
// get pattern and check if the output will be with count
var outputWithCount = helper.withCount(params); 
var pattern = helper.getFilter(params);
// log result
console.log(JSON.stringify(helper.getFiltredData(data.data,pattern,outputWithCount),null,4));