var helper = require('./../helper');

var data = [
    {
        name: 'France',
        people:
          [{
            name: 'Lionardo Graham',
            animals:
              [{name: 'Liona'},
                {name: 'Duck'},
                {name: 'Narwhal'},
                {name: 'Badger'},
                {name: 'Cobra'},
                {name: 'Crow'}]
          },
          {
            name: 'Raphael',
            animals:
              [
                {name: 'Dana'}]
          }
        ]
    },
    {
        name: 'Belgique',
        people:
          [{
            name: 'Lionel Michael',
            animals:
              [
                {name: 'Lion'},
                {name: 'Crow'}]
          }
        ]
    },
    {
        name: 'Spain',
        people:
          [{
            name: 'Louis Enrique',
            animals:
              [
                {name: 'Roky'},
                {name: 'Crow'}]
          }
        ]
    }
];


test('withCount test', () => {
    var params = ["","--param"];
    expect(helper.withCount(params)).toBe(false);
    params.push("--count");
    expect(helper.withCount(params)).toBe(true);
});


test('getFilter test', () => {
    var params = ['','--count','--param'];
    expect(()=> {helper.getFilter(params)}).toThrow();
    params.push('--filter');
    expect(()=> {helper.getFilter(params)}).toThrow();
    params.push('--filter=pattern');
    expect(helper.getFilter(params)).toBe('pattern');
});

test('getFilteredData without count test', () => {
    var pattern = 'Li';
    // first - execution
    var result = helper.getFiltredData(data,pattern);
    // test rest 
    expect(result).toBeDefined();
    expect(result).toHaveLength(2);
    // test people
    expect(result[0]).toBeDefined()
    expect(result[0].people).toBeDefined();
    expect(result[0].people).toHaveLength(1);
    // test animals
    expect(result[0].people[0].animals).toBeDefined();
    expect(result[0].people[0].animals).toHaveLength(1);
});


test('getFilteredData with count test', () => {
    var pattern = 'Li';
    // first - execution
    var result = helper.getFiltredData(data,pattern,true);
    // test result
    expect(result).toBeDefined();
    expect(result).toHaveLength(2);
    // test people
    expect(result[0]).toBeDefined()
    expect(result[0].people).toBeDefined();
    expect(result[0].people).toHaveLength(1);
    expect(result[1]).toBeDefined()
    expect(result[1].people).toBeDefined();
    expect(result[1].people).toHaveLength(1);
    // test country name
    expect(result[0].name).toBe(data[0].name + ' [1]');
    expect(result[1].name).toBe(data[1].name + ' [1]');
    // test animals
    expect(result[0].people[0].animals).toBeDefined();
    expect(result[0].people[0].animals).toHaveLength(1);
    expect(result[1].people[0].animals).toBeDefined();
    expect(result[1].people[0].animals).toHaveLength(1);
    // test people name
    expect(result[0].people[0].name).toBe( data[0].people[0].name + ' [1]');
    expect(result[1].people[0].name).toBe( data[1].people[0].name + ' [1]');
});